# DicomEdit Pixels Dcm4che #

This is meant to be an implementation of a PixelEditHandler based on Dcm4che 5 libraries.

The goal is to replace the implementation in dicomedit-pixels which is based on the pixelmed library. The pixelmed 
library is yet another DICOM library dependency and was used to get basic functionality quickly.

This library was in the middle of development when development stopped.

### Build ###

mvn clean build