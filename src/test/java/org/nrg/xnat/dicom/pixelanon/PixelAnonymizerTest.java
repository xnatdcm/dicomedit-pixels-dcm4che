package org.nrg.xnat.dicom.pixelanon;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class PixelAnonymizerTest {

    @Test
    public void transcode() {
        try {
            File dst = new File( "/tmp/transcode.dcm");
            PixelAnonymizer anonymizer = new PixelAnonymizer();
            File src = new File(anonymizer.getClass().getClassLoader().getResource("US-RGB-8-esopecho.dcm").toURI());
            anonymizer.transcodeWithTranscoder(src, dst);
        }
        catch( Exception e) {
            e.printStackTrace();
        }
    }


}