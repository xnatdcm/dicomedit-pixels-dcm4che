package org.nrg.xnat.dicom.pixelanon;

import org.junit.Test;

import java.awt.*;
import java.io.File;

import static org.junit.Assert.fail;

public class DicomImageTranscoderTest {

    @Test
    public void test() {
        try {
            File file = new File(this.getClass().getClassLoader().getResource("US-RGB-8-esopecho.dcm").toURI());
            DicomImageTranscoder dicomImage = new DicomImageTranscoder(file);

            BlankRegionPixelProcessor pixelProcessor = new BlankRegionPixelProcessor(50, 50, 30, 30, Color.WHITE);
            dicomImage.alterPixels(0, pixelProcessor);

            dicomImage.write( new File("/tmp/pray2.dcm"));

        } catch (Exception e) {
            fail("Unexpected exception: " + e);
        }
    }

}
