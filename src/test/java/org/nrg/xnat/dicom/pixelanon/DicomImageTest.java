package org.nrg.xnat.dicom.pixelanon;

import org.junit.Test;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

import static org.junit.Assert.*;

public class DicomImageTest {

    @Test
    public void test() {
        try {
            File file = new File( this.getClass().getClassLoader().getResource("US-RGB-8-esopecho.dcm").toURI());
            DicomImage dicomImage = new DicomImage( file);

            BlankRegionPixelProcessor pixelProcessor = new BlankRegionPixelProcessor(50, 50, 30, 30, Color.WHITE);
            dicomImage.alterPixels( 0, pixelProcessor);

//            BufferedImage bi = dicomImage.getFrame(0).get();
//            Graphics2D g2d = bi.createGraphics();
//            g2d.setComposite( AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
//            g2d.setColor( Color.WHITE);
//            g2d.fillRect( 50, 50, 30, 30);
//            g2d.dispose();
//
//            dicomImage.setPixels( bi);

            dicomImage.save( new File( "/tmp/pray2.dcm"));

        }
        catch( Exception e) {
            fail( "Unexpected exception: " + e);
        }
    }

}