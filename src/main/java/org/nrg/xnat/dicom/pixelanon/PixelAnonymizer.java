package org.nrg.xnat.dicom.pixelanon;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.imageio.codec.Transcoder;
import org.dcm4che3.io.DicomEncodingOptions;
import org.dcm4che3.util.Property;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class PixelAnonymizer extends DicomImageRenderer {

    public File scrub( File input, int frame, File dst) throws IOException {

        BufferedImage bi = readImage( input, frame);

        writeImage( dst, bi);
        return dst;
    }

    public void transcodeWithTranscoder(File src, final File dest) throws IOException {
        String tsuid ="1.2.840.10008.1.2.2";
        DicomEncodingOptions encOpts = DicomEncodingOptions.DEFAULT;
        final List<Property> params = new ArrayList<Property>();

        try (Transcoder transcoder = new Transcoder(src)) {
            transcoder.setIncludeFileMetaInformation( true);
            transcoder.setRetainFileMetaInformation( false);
            transcoder.setEncodingOptions(encOpts);
            transcoder.setDestinationTransferSyntax(tsuid);
            transcoder.setCompressParams(params.toArray(new Property[params.size()]));
            transcoder.transcode(new Transcoder.Handler(){
                @Override
                public OutputStream newOutputStream(Transcoder transcoder, Attributes dataset) throws IOException {
                    return new FileOutputStream(dest);
                }
            });
        } catch (Exception e) {
            Files.deleteIfExists(dest.toPath());
            throw e;
        }
    }


}
