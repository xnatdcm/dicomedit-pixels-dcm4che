package org.nrg.xnat.dicom.pixelanon;

import java.awt.image.BufferedImage;

@FunctionalInterface
public interface PixelProcessor {
    void process( BufferedImage bufferedImage);
}
