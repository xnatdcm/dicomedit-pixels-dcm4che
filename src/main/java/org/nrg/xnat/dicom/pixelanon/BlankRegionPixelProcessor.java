package org.nrg.xnat.dicom.pixelanon;

import java.awt.*;
import java.awt.image.BufferedImage;

public class BlankRegionPixelProcessor implements PixelProcessor {
    private int x, y, w, h;
    private Color c;

    public BlankRegionPixelProcessor( int x, int y, int w, int h, Color c) {
        setArgs( x, y, w, h, c);
    }
    public void setArgs( int x, int y, int w, int h, Color c) {
        this.x = x; this.y = y; this.w = w; this.h = h; this.c = c;
    }

    public void process( BufferedImage bi) {
        Graphics2D g2d = bi.createGraphics();
        g2d.setComposite( AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
        g2d.setColor( c);
        g2d.fillRect( x, y, w, h);
        g2d.dispose();
    }

}
