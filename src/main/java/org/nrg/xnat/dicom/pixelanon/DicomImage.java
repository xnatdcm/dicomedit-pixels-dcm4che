package org.nrg.xnat.dicom.pixelanon;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.data.UID;
import org.dcm4che3.data.VR;
import org.dcm4che3.imageio.codec.ImageDescriptor;
import org.dcm4che3.imageio.plugins.dcm.DicomImageReadParam;
import org.dcm4che3.imageio.plugins.dcm.DicomImageReader;
import org.dcm4che3.imageio.plugins.dcm.DicomImageReaderSpi;
import org.dcm4che3.imageio.plugins.dcm.DicomMetaData;
import org.dcm4che3.io.DicomInputStream;
import org.dcm4che3.io.DicomOutputStream;

import javax.imageio.stream.FileImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class DicomImage {
    private DicomMetaData dicomMetaData;
    private Attributes dataset;
    private BufferedImage bufferedImage;
    private DicomImageReader dicomImageReader;
    private DicomImageReadParam dicomImageReadParam;
    private int nFrames;
    private Map<Integer, BufferedImage> modifiedFrames;

    public DicomImage( File inputFile) throws IOException {
        dicomImageReader = new DicomImageReader( new DicomImageReaderSpi());
        dicomImageReadParam = new DicomImageReadParam();
        dicomImageReader.setInput( new FileImageInputStream( inputFile));
        nFrames = dicomImageReader.getNumImages(true);
        dicomMetaData = dicomImageReader.getStreamMetadata();
        dataset = dicomMetaData.getAttributes();
        modifiedFrames = new HashMap<>();
    }

    public int getFrameCount() {
        return nFrames;
    }

    Optional<BufferedImage> getFrame( int frameIndex) throws IOException {
        DicomMetaData dicomMetaData = dicomImageReader.getStreamMetadata();
        return Optional.ofNullable( dicomImageReader.read( frameIndex, dicomImageReadParam));
    }

    public void setPixels( BufferedImage bufferedImage) {
        byte[] bytes = ((DataBufferByte) bufferedImage.getData().getDataBuffer()).getData();
        dataset.setBytes(Tag.PixelData, VR.OB, bytes);
    }

    public void save( File file) throws IOException {
        DicomOutputStream dicomOutputStream = new DicomOutputStream( file);
        String tsuid = dicomMetaData.getTransferSyntaxUID();
        dicomOutputStream.writeDataset( dataset.createFileMetaInformation(UID.ExplicitVRLittleEndian), dataset);
    }

    public void alterPixels( int frameIndex, PixelProcessor pp) throws IOException {
        BufferedImage bufferedImage = dicomImageReader.read(frameIndex, dicomImageReadParam);
        pp.process( bufferedImage);
        modifiedFrames.put( frameIndex, bufferedImage);
    }

//    private void decompressPixelData() throws IOException {
//        ImageDescriptor imageDescriptor = new ImageDescriptor( dicomMetaData.getAttributes());
//        int length = imageDescriptor.getLength();
//        int padding = length & 1;
//        adjustDataset();
//        writeDataset();
//        dos.writeHeader(Tag.PixelData, VR.OW, length + padding);
//        for (int i = 0; i < imageDescriptor.getFrames(); i++) {
//            decompressFrame(i);
//
//            pixelProcessor.process( originalBi);
//
//            writeFrame();
//        }
//        if (padding != 0)
//            dos.write(0);
//    }
//
//    private void writeDataset() throws IOException {
//        Attributes fmi = null;
//        if (includeFileMetaInformation) {
//            if (retainFileMetaInformation)
//                fmi = dis.getFileMetaInformation();
//            if (fmi == null)
//                fmi = dataset.createFileMetaInformation(destTransferSyntax);
//            else
//                fmi.setString(Tag.TransferSyntaxUID, VR.UI, destTransferSyntax);
//        }
//        dos.writeDataset(fmi, dataset);
//        fileMetaInformation = fmi;
//    }

}
