package org.nrg.xnat.dicom.pixelanon;

import org.dcm4che3.data.Tag;
import org.dcm4che3.data.UID;
import org.dcm4che3.data.VR;
import org.dcm4che3.imageio.codec.Transcoder;
import org.dcm4che3.imageio.codec.TransferSyntaxType;
import org.dcm4che3.io.DicomEncodingOptions;
import org.dcm4che3.io.DicomInputStream;
import org.dcm4che3.io.DicomOutputStream;
import org.dcm4che3.util.Property;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class DicomImageTranscoder extends Transcoder {
    private Map<Integer, BufferedImage> originalFrames;
    private Map<Integer, BufferedImage> modifiedFrames;

    public DicomImageTranscoder(File f) throws IOException {
        super(f);
        init();
    }

    public DicomImageTranscoder(InputStream in) throws IOException {
        super(in);
        init();
    }

    public DicomImageTranscoder(InputStream in, String tsuid) throws IOException {
        super(in, tsuid);
        init();
    }

    public DicomImageTranscoder(DicomInputStream dis) throws IOException {
        super(dis);
        init();
    }

    private void init() throws IOException {
        originalFrames = new HashMap<>();
        modifiedFrames = new HashMap<>();

        setIncludeFileMetaInformation( true);
        setRetainFileMetaInformation( true);
        setEncodingOptions( DicomEncodingOptions.DEFAULT);
//        setDestinationTransferSyntax(tsuid);
//        setCompressParams(params.toArray(new Property[params.size()]));

        readAllAttributes();

//        processPixelData();
    }

    public void alterPixels( int frameIndex, PixelProcessor pixelProcessor) {
        pixelProcessor.process( getFrame( frameIndex));
    }

    public void decompressPixelData() throws IOException {

        if( getDecompressor() != null) adjustDataset();

        for(int i = 0; i < this.getImageDescriptor().getFrames(); ++i) {
            if( getDecompressor() == null) {
                originalFrames.put(i, readNewFrame());
            }
            else {
                originalFrames.put(i, decompressFrame(i));
            }
        }
    }

    private BufferedImage getFrame( int frameIndex) {
        if( modifiedFrames.containsKey( frameIndex)) {
            return modifiedFrames.get( frameIndex);
        }
        else if( originalFrames.containsKey( frameIndex)) {
            return originalFrames.get( frameIndex);
        }
        else {
            throw new IllegalArgumentException( "Unknown frame with index " + frameIndex);
        }
    }

    public void write( File f) throws IOException {
        setDestinationTransferSyntax( UID.ExplicitVRLittleEndian);
        DicomOutputStream dos = new DicomOutputStream( f);

    }

    public void writeUncompressed( DicomOutputStream dos) throws IOException {
        int length = getImageDescriptor().getLength();
        int padding = length & 1;
        adjustDataset();
        writeDataset();
        dos.writeHeader(Tag.PixelData, VR.OW, length + padding);
        for (int i = 0; i < getImageDescriptor().getFrames(); i++) {
            decompressFrame(i);
            writeFrame( getFrame( i));
        }
        if (padding != 0)
            dos.write(0);
    }


    // called by readAllAttributes.
    public void processPixelData() throws IOException {
        if (getDecompressor() != null) {
            this.initEncapsulatedPixelData();
        }
        decompressPixelData();
    }

//    public void transcode(Handler handler) throws IOException {
//
//        this.handler = handler;
//        dis.readAllAttributes(dataset);
//
//        if (dos == null) {
//            if (compressor != null) { // Adjust destination Transfer Syntax if no pixeldata
//                destTransferSyntax = UID.ExplicitVRLittleEndian;
//                destTransferSyntaxType = TransferSyntaxType.NATIVE;
//                lossyCompression = false;
//            }
//            initDicomOutputStream();
//            writeDataset();
//        } else
//            dataset.writePostPixelDataTo(dos);
//    }

}
